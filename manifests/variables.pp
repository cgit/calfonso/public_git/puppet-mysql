class variables {
    $mysql_root_user                   = "root" 
    $mysql_root_database               = "mysql"
    $mysql_root_local_host             = "localhost"
    $mysql_global_host                 = "%"

    # Replication MySQL User
    $mysql_replication_user            = "replication"

    # Replication MySQL Server ID's
    $mysql_master_server_id            ="1"
    $mysql_slave_server_id             ="2"
}
