#!/bin/bash

PATH=$PATH

logname=$(mysql -u replication --host=dmb1-jboss-mysql.usersys.redhat.com --password=password  --execute="show master status"  | grep mysqllog | awk '{ print $1 }')
export logname
position=$(mysql -u replication --host=dmb1-jboss-mysql.usersys.redhat.com --password=password  --execute="show master status"  | grep mysqllog | awk '{ print $2 }')
export position

rm /var/lib/mysql/set_master_repl_data.sql

#Build MySQL Master Data Script
echo CHANGE MASTER TO >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_HOST=\'10.11.227.42\', >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_USER=\'replication\', >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_PASSWORD=\'password\', >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_LOG_FILE=\'$logname\', >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_LOG_POS=$position, >> /var/lib/mysql/set_master_repl_data.sql
echo MASTER_CONNECT_RETRY=10\;  >> /var/lib/mysql/set_master_repl_data.sql


/usr/bin/mysql --user='replication' --password='password' --database='mysql' --host='localhost' --execute="source /var/lib/mysql/set_master_repl_data.sql"
