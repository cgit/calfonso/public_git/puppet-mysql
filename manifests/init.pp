# mysql.pp
# Copyright (C) 2007 David Schmitt <david@schmitt.edv-bus.at>
# See LICENSE for the full license granted to you.

import "variables.pp"
import "passwords"

class mysql::server {
    include passwords
    include variables
    
    case $mysql_grade{
        community: {
            $mysql_server_package = "MySQL-server-community"
            $mysql_client_package = "MySQL-client-community"
            $mysql_service_name = "mysql"
        }
        default: {
            $mysql_server_package = "mysql-server"
            $mysql_client_package = "mysql"
            $mysql_service_name = "mysqld"
        }
    }
    group { "mysql":
        ensure     => present,
        require    => User["mysql"],
    }

    user { "mysql":
        ensure     => present,
        require    => Package["mysql-server"],
    }
    
    package { "mysql-client":
        name   => $mysql_client_package,
        ensure => installed,
    }

    package { "mysql-server":
        name   => $mysql_server_package,
        ensure => installed,
        require => Package["mysql-client"],
    }

    service { "mysql":
        name => $mysql_service_name,
        ensure => running,
        hasstatus => true,
        require => Package["mysql-server"],
    }

    # It is more convenient to set the root password with 
    # and exec than using the plugin

    $mysql_root_user       = $variables::mysql_root_user
    $mysql_root_database   = $variables::mysql_root_database
    $mysql_root_password   = $passwords::mysql_root_password
    $mysql_root_local_host = $variables::mysql_root_local_host
    $mysql_global_host     = $variables::mysql_global_host

    $mysql_replication_user = $variables::mysql_replication_user
    $mysql_replication_password = $passwords::mysql_replication_password


    $mysql_cmd_root_without_pwd = "/usr/bin/mysql --user=$mysql_root_user --database=$mysql_root_database --host=$mysql_root_local_host"
    $mysql_cmd_root_with_pwd    = "/usr/bin/mysql --user=$mysql_root_user --database=$mysql_root_database --host=$mysql_root_local_host --password=$mysql_root_password"
    $mysql_cmd_repl_with_pwd    = "/usr/bin/mysql --user=$mysql_replication_user --database=$mysql_root_database --host=$mysql_root_local_host --password=$mysql_replication_password"
    $mysql_cmd_repl_slave ="/usr/bin/mysql --user=$mysql_replication_user --database=$mysql_root_database --host=$mysql_master_ip_address --password=$mysql_replication_password"

    exec { "mysql root password init":
        command     => "$mysql_cmd_root_without_pwd --execute=\"Update user set password=password('$mysql_root_password') where user='$mysql_root_user';\"",
        unless      => "$mysql_cmd_root_with_pwd --execute '\s'",
        require     => Service["mysql"],
    }
        
    exec { "mysql root flush password":
        command     => "$mysql_cmd_root_without_pwd --execute=\"flush privileges;\"",
        unless      => "$mysql_cmd_root_with_pwd --execute '\s'",
        require     => Exec["mysql root password init"],
    }

    exec {"drop blank user with hostname as localhost":
        command     => "$mysql_cmd_root_with_pwd --execute=\"drop user ' '@'$mysql_root_local_host';\"",
        onlyif      => "$mysql_cmd_root_with_pwd --execute=\"select * from user where user = ' ';\" | grep $mysql_root_local_host",
        require     => Exec["mysql root flush password"],
    }
   
    exec {"drop blank user with hostname as hqdn facter":
        command     => "$mysql_cmd_root_with_pwd --execute=\"drop user ' '@'$fqdn';\"",
        onlyif      => "$mysql_cmd_root_with_pwd --execute=\"select *  from user where user = ' ';\" | grep $fqdn",
        require     => Exec["mysql root flush password"],
    }

    exec {"mysql create replication user":
        command     => "$mysql_cmd_root_with_pwd --execute=\"Create user '$mysql_replication_user'@'$mysql_global_host' identified by '$mysql_replication_password';\"",
        unless      => "$mysql_cmd_repl_with_pwd --execute '\s'",
        require     => [Exec["drop blank user with hostname as localhost"],Exec["drop blank user with hostname as hqdn facter"]],
    }

    exec { "grants all to replication user":
        command     => "$mysql_cmd_root_with_pwd --execute=\"GRANT All PRIVILEGES  ON *.* TO '$mysql_replication_user'@'$mysql_global_host' IDENTIFIED BY '$mysql_replication_password';\"",
        unless      => "$mysql_cmd_repl_with_pwd --execute '\s'",
        require     => Exec["mysql create replication user"],
    }
}

class mysql::standalone inherits mysql::server {
    mysql::mysql_config {"set binary logging":
        binary_logging => false,
    }
}

class mysql::m2s inherits mysql::server {
    mysql::mysql_config {"set binary logging":
        binary_logging => true,
    } 
}

class mysql::slave inherits mysql::server {
    mysql::mysql_config {"set binary logging":
        binary_logging => false,
    }

    mysql::mysql_replication {"Configure Slave Server for Replication":
    }
}

class mysql::m2m inherits mysql::server {
    mysql::mysql_config {"set binary logging":
        binary_logging => true,
    }

    mysql::mysql_replication {"Configure Master Server for Master to Master Replication":
    }
}

define mysql::mysql_config ($binary_logging){
    case $mysql_grade{
        community: {
            $mysql_config = "mysql/my.cnf.erb"
        }
        default: {
            $mysql_config = "mysql/my.cnf.5.0.erb"
        }
    }
    file { "/etc/my.cnf":
        ensure    => present,
        owner     => "mysql", 
        group     => "mysql", 
        mode      => 0644, 
        content   => template("$mysql_config"),
        require   => [Service["mysql"],Exec["grants all to replication user"]]
    }
    exec { "restart mysql server":
        command     => "service $mysql_service_name restart",
        unless      => "$mysql_cmd_repl_with_pwd --execute=\"show master status;\" | grep mysqllog",
        require     => File["/etc/my.cnf"],
    }
}

define mysql::mysql_replication {
    file { "/var/lib/mysql/gather_master_data.bash":
        owner       => "mysql",
        group       => "mysql",
        ensure      => present,
        mode        => 0755,
        content     => template("mysql/gather_master_data.bash.erb"),
        require     => Exec["restart mysql server"],
    }

    exec { "get master data for slave":
        command     => "/var/lib/mysql/gather_master_data.bash",
        creates     => "/var/lib/mysql/set_master_repl_data.sql",
        onlyif      => "$mysql_cmd_repl_slave --execute=\"show master status;\" | grep mysqllog",
        require     => File["/var/lib/mysql/gather_master_data.bash"],
    }

    # Restart local slave server
    exec { "restart slave server":
        command     => "$mysql_cmd_repl_with_pwd --execute=\"slave stop; slave start\"",
        unless      => "$mysql_cmd_repl_with_pwd --execute=\"show slave status;\" | grep Wait",
        require     => Exec["get master data for slave"],
    }
}

define mysql::datasource($rootpw, $ds_name, $ds_owner, $ds_owner_pwd, $ds_user, $ds_user_pwd, $ds_schema, $mysql_replication_user, $mysql_replication_password, $mysql_root_database, $mysql_root_local_host, $ds_owner_permissions, $ds_user_permissions) {
    case $mysql_type {
        standalone: {
            $mysql_root_cmd = "/usr/bin/mysql --user=root --password=$rootpw "
               
            exec { "create datasource $ds_name":
                command     => "/usr/bin/mysqladmin -u root -p$rootpw create $ds_name",
                unless      => "$mysql_root_cmd $ds_name --execute='\s'",
                require     => [Service["mysql"], Exec["restart mysql server"]],
            }

            exec { "create grants $ds_name":
                command     => "$mysql_root_cmd --database=$mysql_root_database  --execute=\"GRANT ${ds_owner_permissions} ON *.* TO '$ds_owner'@'%' IDENTIFIED BY '$ds_owner_pwd' WITH GRANT OPTION;\"",
                unless      => "/usr/bin/mysql --host=$ipaddress --user=$ds_owner --password=$ds_owner_pwd  --database=$mysql_root_database --execute='\s'",
                require     => Exec["create datasource $ds_name"],
            }

            exec { "create grants $ds_user":
                command     => "$mysql_root_cmd --database=$mysql_root_database --execute=\"GRANT ${ds_user_permissions} ON $ds_name.* TO '$ds_user'@'%' IDENTIFIED BY '$ds_user_pwd';\"",
                unless      => "/usr/bin/mysql --host=$ipaddress --user=$ds_user --password=$ds_user_pwd --database=$mysql_root_database --execute='\s'",
                require     => Exec["create grants $ds_name"],
            }
    
            # Only create the schema is a template directory was specified
            if $ds_schema {        
                exec { "create db $ds_name":
                    command     => "$mysql_root_cmd $ds_name < $ds_schema > /var/lib/mysql/${ds_name}-create-db.log",
                    creates     => "/var/lib/mysql/${ds_name}-create-db.log",
                    onlyif      => "$mysql_root_cmd --database=$mysql_root_database  --execute='\s'",
                    require     => Exec["create grants $ds_user"],
                }         
            }
        }
        primary-master: {
            $mysql_root_cmd ="/usr/bin/mysql --user=root --password=$rootpw  "
            $mysql_cmd_repl_slave ="/usr/bin/mysql --user=$mysql_replication_user --database=$mysql_root_database --host=$mysql_master_ip_address --password=$mysql_replication_password"

            file { "/var/lib/mysql/${ds_name}_verify_slave_configuration.bash":
                ensure      => present,
                owner       => "mysql",
                group       => "mysql",
                mode        => 0755,
                content     => template("mysql/verify_slave_configuration.bash.erb"),
                require     => [Service["mysql"], Exec["restart slave server"]],
            }

            exec { "create datasource $ds_name":
                command     => "/usr/bin/mysqladmin -u root -p$rootpw create $ds_name",
                onlyif      => "/var/lib/mysql/${ds_name}_verify_slave_configuration.bash",
                require     => File["/var/lib/mysql/${ds_name}_verify_slave_configuration.bash"],
            }

            exec { "create all grants $ds_name":
                command     => "$mysql_root_cmd --database=$mysql_root_database --execute=\"GRANT ${ds_owner_permissions} ON *.* TO '$ds_owner'@'%' IDENTIFIED BY '$ds_owner_pwd' WITH GRANT OPTION;\"",
                creates     => "/var/lib/mysql/'$ds_name'-all-grants-created.out",
                unless      => "$mysql_cmd_repl_slave  --execute=\"select user from user;\" | grep '$ds_owner'",
                require     => Exec["create datasource $ds_name"], 
            }

            exec { "create select grants $ds_user":
                command     => "$mysql_root_cmd --database=$mysql_root_database --execute=\"GRANT ${ds_user_permissions} ON $ds_name.* TO '$ds_user'@'%' IDENTIFIED BY '$ds_user_pwd';\"",
                creates     => "/var/lib/mysql/'$ds_name'-select-grants-created.out",
                unless      => "$mysql_cmd_repl_slave --execute=\"select user from user;\" | grep '$ds_user'", 
                require     => Exec["create all grants $ds_name"], 
            }
    
            # Only create the schema is a template directory was specified
            if $ds_schema {
                exec { "create db schema $ds_name":
                    command     => "$mysql_root_cmd --database=$ds_name < $ds_schema > /var/lib/mysql/${ds_name}-create-db.log",
                    creates     => "/var/lib/mysql/${ds_name}-create-db.log",
                    onlyif      =>  "$mysql_root_cmd --database=$mysql_root_database  --execute='\s'",
                    require     => Exec["create select grants $ds_user"],  
                }
            }
        }
    }
}
